// let callCount = 0  //REMOVE
function toStringThis(thingy){
    let outputStr = []
    // ++callCount  //REMOVE
    // let outputStr = '"'
    if(thingy == null){
        outputStr.push('null')
    } else if(thingy.constructor.name == 'Array'){
        outputStr.push('[')
        for(let i=0;i<thingy.length;i++){
            if(typeof thingy[i].constructor.name == 'Array'){
                for(let j=0;j<thingy[i].length;j++){
                    // outputStr.push(thingy[i][j]+',')
                    outputStr.push(thingy[i][j])
                }
            } else if(typeof thingy[i]== 'object'){
                let curItem = Object.keys(thingy[i])
                outputStr.push('{')
                for(let j=0;j<curItem.length;j++){
                    if(typeof thingy[i][curItem[j]]=='string'){
                        // outputStr.push(`'${curItem[j]}':'${toStringThis(thingy[i][curItem[j]])}'`)
                        outputStr.push(`'${curItem[j]}':'${toStringThis(thingy[i][curItem[j]])}'`)
                    } else {
                        // outputStr.push(`'${curItem[j]}':${toStringThis(thingy[i][curItem[j]])}`)
                        outputStr.push(`'&&${curItem[j]}':${toStringThis(thingy[i][curItem[j]])}&&`)
                        // outputStr.push(`'${curItem[j]}':`)
                        // outputStr = [...outputStr, ...toStringThis(thingy[curItem[j]]), ``]
                    }
                }
                outputStr.push('}')
                // outputStr.push('},')
            } else if(typeof thingy[i]== 'string'){
                // outputStr.push(`${thingy[i]},`)
                // outputStr.push(`${thingy[i]}`)
                outputStr.push(thingy[i])
            } else {
                // outputStr.push(`${thingy[i]},`)
                outputStr.push(`${thingy[i]}`)
            }
        }
        outputStr.push(']')
    } else if (typeof thingy == 'object') {
        let curItem = Object.keys(thingy)
        outputStr.push('{')
        for(let j=0;j<curItem.length;j++){
            if(curItem.length-1==j){
                // outputStr.push(`'${curItem[j]}':'${toStringThis(thingy[curItem[j]])}'`)
                outputStr.push(`'${curItem[j]}':`)
                outputStr = [...outputStr, ...toStringThis(thingy[curItem[j]])]
            } else {
                // outputStr.push(`'${curItem[j]}':${toStringThis(thingy[curItem[j]])}',`)
                outputStr.push(`'${curItem[j]}':`)
                // outputStr = [...outputStr, ...toStringThis(thingy[curItem[j]]), `,`]
                outputStr = [...outputStr, ...toStringThis(thingy[curItem[j]])]
            }
        }
        // outputStr.push('},')
        outputStr.push('}')
    } else if(typeof thingy =='string') {
        outputStr.push("'"+thingy+"'")
        // outputStr.push(thingy)
    } else {
        // outputStr.push(thingy+',')
        outputStr.push(thingy)
    }
    // outputStr+='"'
    // console.log('callcount', callCount)  //REMOVE
    // --callCount  //REMOVE

    // console.log('it',typeof outputStr, outputStr)  //REMOVE
    return outputStr
}


export default async function (item){
    try {
        return await toStringThis(item)
    } catch (err){
        return err
    }
}
// let callCount = 0  //REMOVE
function toStringThis(thingy){
    let outputStr = ''
    // ++callCount  //REMOVE
    // let outputStr = '"'
    if(thingy == null){
        outputStr+='null,'
    } else if(thingy.constructor.name == 'Array'){
        outputStr+='['
        for(let i=0;i<thingy.length;i++){
            if(typeof thingy[i].constructor.name == 'Array'){
                for(let j=0;j<thingy[i].length;j++){
                    outputStr+=thingy[i][j]+','
                }
            } else if(typeof thingy[i]== 'object'){
                let curItem = Object.keys(thingy[i])
                outputStr+='{'
                for(let j=0;j<curItem.length;j++){
                    if(typeof thingy[i][curItem[j]]=='string'){
                        outputStr+=`'${curItem[j]}':'${toStringThis(thingy[i][curItem[j]])}'`
                    } else {
                        outputStr+=`'${curItem[j]}':${toStringThis(thingy[i][curItem[j]])}`
                    }
                }
                outputStr+='},'
            } else if(typeof thingy[i]== 'string'){
                outputStr+=`'${thingy[i]}',`
            } else {
                outputStr+=`${thingy[i]},`
            }
        }
        outputStr+=']'
    } else if (typeof thingy == 'object') {
        let curItem = Object.keys(thingy)
        outputStr+='{'
        for(let j=0;j<curItem.length;j++){
            if(curItem.length-1==j){
                outputStr+=`'${curItem[j]}':'${toStringThis(thingy[curItem[j]])}'`
            } else {
                outputStr+=`'${curItem[j]}':${toStringThis(thingy[curItem[j]])},`
            }
        }
        outputStr+='},'
    } else if(typeof thingy =='string') {
        outputStr+="'"+thingy+"'"
    } else {
        outputStr+=thingy+','
    }
    // outputStr+='"'
    // console.log('callcount', callCount)  //REMOVE
    // --callCount  //REMOVE

    // console.log('it',typeof outputStr, outputStr)  //REMOVE
    return outputStr
}


export default async function (item){
    try {
        return await toStringThis(item)
    } catch (err){
        return err
    }
}